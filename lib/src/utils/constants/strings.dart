class Strings {
  // App
  static const String appTitle = 'Flutter Clean Architecture';

  // Networking and APIs
  static const String baseUrl = 'https://newsapi.org/v2';
  static const String defaultApiKey = 'ff957763c54c44d8b00e5e082bc76cb0';
  static const String defaultSources = 'bbc-news, abc-news, al-jazeera-english';

  // Storage and Databases
  static const String articlesTableName = 'articles_table';
  static const String databaseName = 'app_database.db';

}