import 'package:dio/dio.dart';
import 'package:retrofit/dio.dart';
import 'package:retrofit/http.dart';

import 'package:flutter_clean_architecture/src/domain/models/responses/breaking_news.response.dart';
import 'package:flutter_clean_architecture/src/utils/constants/strings.dart';

part 'news_api_service.g.dart';

@RestApi(baseUrl: Strings.baseUrl, parser: Parser.JsonSerializable)
abstract class NewsApiService {
  factory NewsApiService(Dio dio, {String baseUrl}) = _NewsApiService;

  @GET("/top-headlines")
  Future<HttpResponse<BreakingNewsResponse>> getBreakingNewsArticles({
    @Query("apiKey") String? apiKey,
    @Query("sources") String? sources,
    @Query("page") int? page,
    @Query("pageSize") int? pageSize,
  });
}
