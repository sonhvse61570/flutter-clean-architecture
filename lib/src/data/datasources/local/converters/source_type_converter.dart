import 'package:floor/floor.dart';
import 'package:flutter_clean_architecture/src/domain/models/source.dart';

class SourceTypeConverter extends TypeConverter<Source?, String> {
  @override
  Source? decode(String databaseValue) {
    final List<String> results = databaseValue.split(',');
    return Source(id: results.first, name: results.last);
  }

  @override
  String encode(Source? value) => '${value?.id}, ${value?.name}';
}
