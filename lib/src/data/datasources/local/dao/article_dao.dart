import 'package:floor/floor.dart';
import 'package:flutter_clean_architecture/src/domain/models/article.dart';
import 'package:flutter_clean_architecture/src/utils/constants/strings.dart';

@dao
abstract class ArticleDao {
  @Query('SELECT * FROM ${Strings.articlesTableName}')
  Future<List<Article>> getAllArticles();

  @Insert(onConflict: OnConflictStrategy.replace)
  Future<void> insertArticle(Article article);

  @delete
  Future<void> deleteArticle(Article article);
}