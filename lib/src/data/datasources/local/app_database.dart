import 'dart:async';

import 'package:floor/floor.dart';
import 'package:sqflite/sqflite.dart' as sqflite;

import 'package:flutter_clean_architecture/src/data/datasources/local/converters/source_type_converter.dart';
import 'package:flutter_clean_architecture/src/data/datasources/local/dao/article_dao.dart';
import 'package:flutter_clean_architecture/src/domain/models/article.dart';

part 'app_database.g.dart';

@TypeConverters([SourceTypeConverter])
@Database(version: 1, entities: [Article])
abstract class AppDatabase extends FloorDatabase {
  ArticleDao get articleDao;
}