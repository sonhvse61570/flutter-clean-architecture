import 'package:flutter_clean_architecture/src/data/datasources/local/app_database.dart';
import 'package:flutter_clean_architecture/src/domain/models/article.dart';
import 'package:flutter_clean_architecture/src/domain/repositories/database_repository.dart';

class DatabaseRepositoryImpl implements DatabaseRepository {
  final AppDatabase _appDatabase;

  DatabaseRepositoryImpl(this._appDatabase);

  @override
  Future<List<Article>> getSavedArticles() =>
      _appDatabase.articleDao.getAllArticles();

  @override
  Future<void> removeArticle(Article article) =>
      _appDatabase.articleDao.deleteArticle(article);

  @override
  Future<void> saveArticle(Article article) =>
      _appDatabase.articleDao.insertArticle(article);
}
