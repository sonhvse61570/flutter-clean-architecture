import 'package:equatable/equatable.dart';
import 'package:flutter_clean_architecture/src/domain/models/article.dart';
import 'package:json_annotation/json_annotation.dart';

part 'breaking_news.response.g.dart';

@JsonSerializable()
class BreakingNewsResponse extends Equatable {
  @JsonKey(name: "status")
  final String status;
  @JsonKey(name: "totalResults")
  final int totalResults;
  @JsonKey(name: "articles")
  final List<Article> articles;

  BreakingNewsResponse({
    required this.status,
    required this.totalResults,
    required this.articles,
  });

  // factory BreakingNewsResponse.fromMap(Map<String, dynamic> map) {
  //   return BreakingNewsResponse(
  //     status: (map['status'] ?? '') as String,
  //     totalResults: (map['totalResults'] ?? 0) as int,
  //     articles: List<Article>.from(
  //       map['articles'].map<Article>(
  //         (x) => Article.fromMap(x as Map<String, dynamic>),
  //       ),
  //     ),
  //   );
  // }

  factory BreakingNewsResponse.fromJson(Map<String, dynamic> json) => _$BreakingNewsResponseFromJson(json);

  Map<String, dynamic> toJson() => _$BreakingNewsResponseToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [status, totalResults, articles];
}
