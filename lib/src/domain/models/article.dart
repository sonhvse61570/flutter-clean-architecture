import 'package:equatable/equatable.dart';
import 'package:floor/floor.dart';
import 'package:flutter_clean_architecture/src/utils/constants/strings.dart';
import 'package:json_annotation/json_annotation.dart';

import 'source.dart';

part 'article.g.dart';

@JsonSerializable()
@Entity(tableName: Strings.articlesTableName)
class Article extends Equatable {
  @JsonKey(name: "id")
  @PrimaryKey(autoGenerate: true)
  final int? id;
  @JsonKey(name: "source")
  final Source? source;
  @JsonKey(name: "author")
  final String? author;
  @JsonKey(name: "title")
  final String? title;
  @JsonKey(name: "description")
  final String? description;
  @JsonKey(name: "url")
  final String? url;
  @JsonKey(name: "urlToImage")
  final String? urlToImage;
  @JsonKey(name: "publishedAt")
  final String? publishedAt;
  @JsonKey(name: "content")
  final String? content;

  const Article({
    this.id,
    this.source,
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
    this.content,
  });

  // factory Article.fromMap(Map<String, dynamic> map) {
  //   return Article(
  //     id: map['id'] != null ? map['id'] as int : null,
  //     source: map['source'] != null
  //         ? Source.fromMap(map['source'] as Map<String, dynamic>)
  //         : null,
  //     author: map['author'] != null ? map['author'] as String : null,
  //     title: map['title'] != null ? map['title'] as String : null,
  //     description:
  //     map['description'] != null ? map['description'] as String : null,
  //     url: map['url'] != null ? map['url'] as String : null,
  //     urlToImage:
  //     map['urlToImage'] != null ? map['urlToImage'] as String : null,
  //     publishedAt:
  //     map['publishedAt'] != null ? map['publishedAt'] as String : null,
  //     content: map['content'] != null ? map['content'] as String : null,
  //   );
  // }

  factory Article.fromJson(Map<String, dynamic> json) => _$ArticleFromJson(json);

  Map<String, dynamic> toJson() => _$ArticleToJson(this);

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      id,
      source,
      author,
      title,
      description,
      url,
      urlToImage,
      publishedAt,
      content,
    ];
  }
}