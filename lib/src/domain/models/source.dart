import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'source.g.dart';

@JsonSerializable()
class Source extends Equatable {
  @JsonKey(name: "id")
  final String? id;
  @JsonKey(name: "name")
  final String? name;

  const Source({
    this.id,
    this.name,
  });

  Source copyWith({
    String? id,
    String? name,
  }) => Source(
    id: id ?? this.id,
    name: name ?? this.name,
  );

  Map<String, dynamic> toMap() => <String, dynamic>{
    'id': id,
    'name': name,
  };

  factory Source.fromMap(Map<String, dynamic> map) => Source(
    id: map['id'] != null ? map['id'] as String : null,
    name: map['name'] != null ? map['name'] as String : null,
  );

  factory Source.fromJson(Map<String, dynamic> json) => _$SourceFromJson(json);

  Map<String, dynamic> toJson() => _$SourceToJson(this);

  @override
  bool? get stringify => true;

  @override
  List<Object?> get props => [id, name];
}
