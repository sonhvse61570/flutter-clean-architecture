import 'package:flutter_clean_architecture/src/utils/constants/nums.dart';
import 'package:flutter_clean_architecture/src/utils/constants/strings.dart';

class BreakingNewsRequest {
  final String apiKey;
  final String sources;
  final int page;
  final int pageSize;

  BreakingNewsRequest({
    this.apiKey = Strings.defaultApiKey,
    this.sources = "bbc-news, abc-news",
    this.page = 1,
    this.pageSize = Nums.defaultPageSize,
  });
}
