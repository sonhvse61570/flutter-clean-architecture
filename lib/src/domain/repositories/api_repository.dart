import 'package:flutter_clean_architecture/src/domain/models/requests/breaking_news.request.dart';
import 'package:flutter_clean_architecture/src/domain/models/responses/breaking_news.response.dart';
import 'package:flutter_clean_architecture/src/utils/resources/data_state.dart';

abstract class ApiRepository {
  Future<DataState<BreakingNewsResponse>> getBreakingNewsArticles({
    required BreakingNewsRequest request,
  });
}
