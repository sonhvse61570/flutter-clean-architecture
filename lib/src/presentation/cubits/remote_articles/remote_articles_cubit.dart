import 'package:flutter_clean_architecture/src/domain/models/article.dart';
import 'package:flutter_clean_architecture/src/domain/models/requests/breaking_news.request.dart';
import 'package:flutter_clean_architecture/src/domain/repositories/api_repository.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/base/base_cubit.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/remote_articles/remote_articles_state.dart';
import 'package:flutter_clean_architecture/src/utils/constants/nums.dart';
import 'package:flutter_clean_architecture/src/utils/resources/data_state.dart';

class RemoteArticlesCubit
    extends BaseCubit<RemoteArticlesState, List<Article>> {
  final ApiRepository _apiRepository;

  RemoteArticlesCubit(this._apiRepository)
      : super(const RemoteArticlesLoading(), []);

  int _page = 1;

  Future<void> getBreakingNewsArticles() async {
    if (isBusy) return;

    await run(() async {
      final response = await _apiRepository.getBreakingNewsArticles(
        request: BreakingNewsRequest(page: _page),
      );

      if (response is DataSuccess) {
        final articles = response.data?.articles ?? [];
        final noMoreData = articles.length < Nums.defaultPageSize;

        data.addAll(articles);
        _page++;

        emit(RemoteArticlesSuccess(articles: data, noMoreData: noMoreData));
      } else if (response is DataFailed) {
        emit(RemoteArticlesFailed(error: response.error));
      }
    });
  }
}
