import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_architecture/src/config/router/app_router.dart';
import 'package:flutter_clean_architecture/src/domain/models/article.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/remote_articles/remote_articles_cubit.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/remote_articles/remote_articles_state.dart';
import 'package:flutter_clean_architecture/src/presentation/widgets/article_widget.dart';
import 'package:flutter_clean_architecture/src/utils/extensions/scroll_controller_extensions.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class BreakingNewsView extends HookWidget {
  const BreakingNewsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final remoteArticlesCubit = BlocProvider.of<RemoteArticlesCubit>(context);
    final scrollController = useScrollController();

    useEffect(() {
      scrollController.onScrollEndsListener(() {
        remoteArticlesCubit.getBreakingNewsArticles();
      });

      return scrollController.dispose;
    }, const []);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Daily News", style: TextStyle(color: Colors.black)),
        actions: [
          GestureDetector(
            onTap: () => appRouter.push(SavedArticlesViewRoute()),
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 14),
              child: Icon(
                Icons.bookmark,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
      body: BlocBuilder<RemoteArticlesCubit, RemoteArticlesState>(
        builder: (BuildContext context, state) {
          switch (state.runtimeType) {
            case RemoteArticlesLoading:
              return const Center(child: CupertinoActivityIndicator());
            case RemoteArticlesFailed:
              return const Center(child: Icon(Icons.refresh));
            case RemoteArticlesSuccess:
              return _buildArticles(
                  scrollController, state.articles, state.noMoreData);
            default:
              return const SizedBox.shrink();
          }
        },
      ),
    );
  }

  Widget _buildArticles(
    ScrollController scrollController,
    List<Article> articles,
    bool noMoreData,
  ) {
    return CustomScrollView(
      controller: scrollController,
      slivers: [
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) => ArticleWidget(
              article: articles[index],
              onArticlePressed: (e) => appRouter.push(ArticleDetailsViewRoute(article: e)),
            ),
            childCount: articles.length,
          ),
        ),
        if (!noMoreData)
          const SliverToBoxAdapter(
            child: Padding(
              padding: EdgeInsets.only(top: 17, bottom: 32),
              child: CupertinoActivityIndicator(),
            ),
          ),
      ],
    );
  }
}
