import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_architecture/src/config/router/app_router.dart';
import 'package:flutter_clean_architecture/src/domain/models/article.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/local_articles/local_articles_cubit.dart';
import 'package:flutter_clean_architecture/src/presentation/widgets/article_widget.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class SavedArticlesView extends HookWidget {
  const SavedArticlesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: GestureDetector(
          behavior: HitTestBehavior.opaque,
          onTap: appRouter.pop,
          child: const Icon(Icons.chevron_left, color: Colors.black,),
        ),
        title: const Text(
          'Saved Articles',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: BlocBuilder<LocalArticlesCubit, LocalArticlesState>(
        builder: (_, state) {
          switch (state.runtimeType) {
            case LocalArticlesLoading:
              return const Center(child: CupertinoActivityIndicator());
            case LocalArticlesSuccess:
              return _buildArticlesList(state.articles);
            default:
              return const SizedBox.shrink();
          }
        },
      ),
    );
  }

  Widget _buildArticlesList(List<Article> articles) {
    if (articles.isEmpty) {
      return const Center(
        child: Text(
          'No Saved Articles',
          style: TextStyle(color: Colors.black),
        ),
      );
    }

    return ListView.builder(
      itemBuilder: (context, index) {
        return ArticleWidget(
          article: articles[index],
          isRemovable: true,
          onRemove: (article) => BlocProvider.of<LocalArticlesCubit>(context).removeArticle(article: article),
          onArticlePressed: (article) => appRouter.push(ArticleDetailsViewRoute(article: article)),
        );
      },
      itemCount: articles.length,
    );
  }
}
