import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_architecture/src/config/router/app_router.dart';
import 'package:flutter_clean_architecture/src/config/themes/app_theme.dart';
import 'package:flutter_clean_architecture/src/domain/repositories/api_repository.dart';
import 'package:flutter_clean_architecture/src/domain/repositories/database_repository.dart';
import 'package:flutter_clean_architecture/src/locator.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/local_articles/local_articles_cubit.dart';
import 'package:flutter_clean_architecture/src/presentation/cubits/remote_articles/remote_articles_cubit.dart';
import 'package:flutter_clean_architecture/src/utils/constants/strings.dart';
import 'package:oktoast/oktoast.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await initializeDependencies();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<LocalArticlesCubit>(
          create: (context) => LocalArticlesCubit(locator<DatabaseRepository>())
            ..getAllSavedArticles(),
        ),
        BlocProvider<RemoteArticlesCubit>(
          create: (context) => RemoteArticlesCubit(locator<ApiRepository>())
            ..getBreakingNewsArticles(),
        ),
      ],
      child: OKToast(
        child: MaterialApp.router(
          debugShowCheckedModeBanner: false,
          routerDelegate: appRouter.delegate(),
          routeInformationParser: appRouter.defaultRouteParser(),
          title: Strings.appTitle,
          theme: AppTheme.light,
        ),
      ),
    );
  }
}
